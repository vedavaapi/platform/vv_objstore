from setuptools import setup

try:
    with open("README.md", "r") as fh:
        long_description = fh.read()
except:
    long_description = ''

setup(
    name='vv_objstore',
    version='1.0.0',
    packages=['vedavaapi', 'vedavaapi.objstore'],
    url='https://github.com/vedavaapi',
    author='vedavaapi',
    description='vedavaapi objstore service',
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=['pymongo', 'flask-restplus', 'libvvobjstore', 'core_services'],
    classifiers=(
            "Programming Language :: Python :: 3.5",
            "Operating System :: OS Independent",
    )
)