from vedavaapi.iiif_presentation.prezed.iiif_model_helper import VedavaapiIIIFModelInterface
from vedavaapi.iiif_image.loris.resolver import VedavaapiFSInterface


def myservice():
    from .. import VedavaapiObjstore
    return VedavaapiObjstore.instance


class ObjstoreIIIFModelInterface(VedavaapiIIIFModelInterface):

    service_name = 'objstore'

    def __init__(self, org_name, colln, json_class_defs):
        super(ObjstoreIIIFModelInterface, self).__init__(org_name, colln, json_class_defs)


class ObjstoreFSInterface(VedavaapiFSInterface):

    service_name = 'objstore'

    def __init__(self, org_name, colln, data_dir_path):
        super(ObjstoreFSInterface, self).__init__(org_name, colln, data_dir_path)
