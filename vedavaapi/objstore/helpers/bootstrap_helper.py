from collections import namedtuple

from vedavaapi.acls.permissions_helper import AclsGenerator, PermissionResolver
from vedavaapi.common.helpers.api_helper import get_initial_agents


InitialResources = namedtuple('InitialResources', ['genesis_resource_id'])


# noinspection PyProtectedMember
def bootstrap_initial_resources(colln, acl_svc, org_name):
    existing_genesis_resource_json = colln.find_one({"_id": "0"}, projection={"_id": 1, "name": 1})
    if existing_genesis_resource_json is not None:
        return InitialResources(existing_genesis_resource_json['_id'])

    initial_agents = get_initial_agents(org_name=org_name)

    genesis_resource_json = {
        "jsonClass": "Library",
        "_id": "0",
        "name": 'Root library',
        "source": None
    }

    inserted_id = colln.insert_one(genesis_resource_json).inserted_id

    acl = AclsGenerator.get_acl_template()
    AclsGenerator.add_to_agent_ids(
        acl, [PermissionResolver.READ], PermissionResolver.GRANT, user_ids=[None],
        team_ids=[initial_agents.all_users_team_id]
    )
    AclsGenerator.add_to_agent_ids(
        acl,
        [PermissionResolver.UPDATE_CONTENT, PermissionResolver.CREATE_CHILDREN, PermissionResolver.CREATE_ANNOS, PermissionResolver.UPDATE_PERMISSIONS],
        PermissionResolver.GRANT, team_ids=[initial_agents.root_admins_team_id]
    )
    acl['_id'] = inserted_id

    acl_svc.update(inserted_id, acl, upsert=True)
    return InitialResources(inserted_id)


def bootstrap_site_object(colln, acl_svc, org_name):
    existing_site_object = colln.find_one({"jsonClass": "VedavaapiSite"}, projection={"_id": 1, "name": 1})
    if existing_site_object is not None:
        return existing_site_object['_id']

    initial_agents = get_initial_agents(org_name=org_name)

    site_obj_json = {
        "jsonClass": "VedavaapiSite",
        #  "_id": "0",
        "name": 'Vedavaapi',
        "description": "a vedavaapi powered site",
    }

    inserted_id = colln.insert_one(site_obj_json).inserted_id

    acl = AclsGenerator.get_acl_template()
    AclsGenerator.add_to_agent_ids(
        acl, [PermissionResolver.READ], PermissionResolver.GRANT, user_ids=['*'],
        team_ids=[initial_agents.all_users_team_id]
    )
    AclsGenerator.add_to_agent_ids(
        acl,
        [PermissionResolver.UPDATE_CONTENT],
        PermissionResolver.GRANT, team_ids=[initial_agents.root_admins_team_id]
    )
    acl['_id'] = inserted_id

    acl_svc.update(inserted_id, acl, upsert=True)
    return inserted_id
