import logging
import os

from collections import namedtuple

from vedavaapi.objectdb.mydb import MyDbCollection
from vedavaapi.common import VedavaapiService, OrgHandler

from .helpers.iiif_helper import ObjstoreFSInterface, ObjstoreIIIFModelInterface
from .helpers.bootstrap_helper import bootstrap_site_object


logging.basicConfig(
    level=logging.INFO,
    format="%(levelname)s: %(asctime)s {%(filename)s:%(lineno)d}: %(message)s "
)


InitialAgents = namedtuple('InitialAgents', ['all_users_team_id', 'root_admins_team_id'])


class ObjstoreOrgHandler(OrgHandler):
    def __init__(self, service, org_name):
        super(ObjstoreOrgHandler, self).__init__(service, org_name)

        self.objstore_db_config = self.dbs_config['objstore_db']
        self.objstore_db = self.store.db(self.objstore_db_config['name'])
        self.objstore_colln = self.objstore_db.get_collection(
            self.objstore_db_config['collections']['objstore']
        )  # type: MyDbCollection

        self.root_dir_path = self.store.file_store_path(
            file_store_type='data',
            base_path=''
        )
        self.initial_agents = InitialAgents('ALL_USERS_TEAM', 'ROOT_ADMINS_TEAM')

    def data_dir_path(self):
        env_path = os.environ.get('OBJSTORE_FILES_DIR')
        if env_path:
            if not os.path.exists(env_path):
                os.makedirs(env_path, exist_ok=True)
            return env_path
        return self.store.file_store_path(
            file_store_type='data',
            base_path=''
        )

    def initialize(self):
        self.objstore_colln.create_index({"source": 1}, 'source_links')
        self.objstore_colln.create_index({'target': 1}, 'target_links')
        self.objstore_colln.create_index({'jsonClass': 1}, 'jsonClass_1')
        self.objstore_colln.create_index({'hierarchy.i': 1, 'hierarchy.e': 1}, 'hierarchy_1')
        # pass
        acl_svc = self.service.registry.lookup('acls').get_acl_svc(self.org_name)
        # noinspection PyAttributeOutsideInit
        self.site_id = bootstrap_site_object(self.objstore_colln, acl_svc, self.org_name)


class VedavaapiObjstore(VedavaapiService):

    instance = None  # type: VedavaapiObjstore

    dependency_services = ['acls']
    org_handler_class = ObjstoreOrgHandler

    title = "Vedavaapi Object Store"
    description = "Object store api"

    def __init__(self, registry, name, conf):
        super(VedavaapiObjstore, self).__init__(registry, name, conf)

    def init_service(self):
        for org_name in self.registry.org_names:
            self.get_org(org_name)  # initializes all orgs.

    def colln(self, org_name):
        return self.get_org(org_name).objstore_colln  # type: MyDbCollection

    def get_accounts_api_config(self, org_name):
        return self.get_org(org_name).accounts_api_config

    def get_initial_agents(self, org_name):
        return self.get_org(org_name).initial_agents

    def set_initial_agents(self, org_name, initial_agents):
        self.get_org(org_name).initial_agents = initial_agents

    def data_dir_path(self, org_name):
        return self.get_org(org_name).data_dir_path()

    def root_dir_path(self, org_name):
        return self.get_org(org_name).root_dir_path

    def iiif_model_interface(self, org_name):
        colln = self.colln(org_name)
        if not colln:
            return None
        vv_schemas = self.registry.lookup('schemas').get_schemas(org_name)
        return ObjstoreIIIFModelInterface(org_name, colln, vv_schemas)

    def fs_interface(self, org_name):
        colln = self.colln(org_name)
        if not colln:
            return None
        data_dir_path = self.data_dir_path(org_name)
        return ObjstoreFSInterface(org_name, colln, data_dir_path)
