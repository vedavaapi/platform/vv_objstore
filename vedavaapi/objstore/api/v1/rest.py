import json
import os
from collections import OrderedDict

import flask
import flask_restx
from flask import request, g
from jsonschema import ValidationError
from vedavaapi.abstractfs import AbstractFileResolver

from vedavaapi.common.helpers.api_helper import jsonify_argument, error_response, check_argument_type, \
    abort_with_error_response
from vedavaapi.common.helpers.args_parse_helper import parse_json_args
from vedavaapi.common.helpers.token_helper import require_oauth, current_token

from vedavaapi.objectdb.helpers import objstore_helper, objstore_graph_helper, projection_helper, ObjModelException
from vedavaapi.acls.permissions_helper import PermissionResolver
from vv_schemas import SchemaValidator

from werkzeug.datastructures import FileStorage

from . import api


def _validate_projection(projection):
    try:
        projection_helper.validate_projection(projection)
    except ObjModelException as e:
        error = error_response(message=e.message, code=e.http_response_code)
        abort_with_error_response(error)


def get_requested_resource_jsons(args):

    json_parse_directives = {
        "selector_doc": {
            "allowed_types": (dict, ), "default": {}
        },
        "projection": {
            "allowed_types": (dict, ), "allow_none": True, "custom_validator": _validate_projection
        },
        "sort_doc": {
            "allowed_types": (dict, list), "allow_none": True
        },
        "attach_in_links": {
            "allowed_types": (bool, ), "default": False
        },
        "return_count_only": {
            "allowed_types": (bool, ), "default": False
        }
    }

    args = parse_json_args(args, json_parse_directives)

    selector_doc = args['selector_doc']
    projection = args['projection']
    sort_doc = args['sort_doc']
    attach_in_links = args['attach_in_links']

    ops = OrderedDict()
    if sort_doc is not None:
        ops['sort'] = [sort_doc]
    if args.get('start', None) is not None:
        ops['skip'] = [args['start']]
    if args.get('count', None) is not None:
        ops['limit'] = [args['count']]

    try:
        total_count = g.objstore_colln.count(selector_doc)
        if args.get('return_count_only', None):
            return {"total_count": total_count, "items": []}
        resource_repr_jsons = objstore_helper.get_read_permitted_resources(
            g.objstore_colln, g.acl_svc, current_token.user_id,
            current_token.team_ids, selector_doc, projection=projection, ops=ops, attach_in_links=attach_in_links
        )
        response_json = {
            "items": resource_repr_jsons,
            "total_count": total_count,
            "selector_doc": args['selector_doc']
        }
        if args['start']:
            response_json['start'] = args['start']
        if args['count']:
            response_json['count'] = args['count']

        return response_json

    except (TypeError, ValueError) as e:
        error = error_response(message='arguments to operations seems invalid', code=400, errors=str(e))
        # raise e
        abort_with_error_response(error)
    except Exception as e:
        error = error_response(message='invalid arguments', code=400, error=str(e))
        abort_with_error_response(error)


#  deprecated
@api.route('/resources')
class Resources(flask_restx.Resource):

    white_listed_classes = ('JsonObject', 'WrapperObject', 'User', 'Team')

    get_parser = api.parser()
    get_parser.add_argument(
        'selector_doc', location='args', type=str, default='{}',
        help='syntax is same as mongo query_doc. https://docs.mongodb.com/manual/tutorial/query-documents/'
    )
    get_parser.add_argument('projection', location='args', type=str, help='ex: {"permissions": 0}')
    get_parser.add_argument('sort_doc', location='args', type=str, help='ex: [["created", 1], ["title.chars", -1]]')
    get_parser.add_argument('start', location='args', type=int)
    get_parser.add_argument('count', location='args', type=int)
    get_parser.add_argument('return_count_only', location='args', type=str, choices=['true', 'false'], default='false')
    get_parser.add_argument(
        'attach_in_links', type=str, location='args', default='false', choices=['true', 'false']
    )
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    # post payload parser
    post_parser = api.parser()
    post_parser.add_argument('resource_jsons', location='form', type=str, required=True)
    post_parser.add_argument('return_projection', location='form', type=str)
    post_parser.add_argument('upsert', location='form', type=str, choices=['true', 'false'], default='false')
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    post_json_parse_directives = {
        "resource_jsons": {"allowed_types": (list, )},
        "return_projection": {
            "allowed_types": (dict, ), "allow_none": True, "custom_validator": _validate_projection
        },
        "upsert": {
            "allowed_types": (bool, ), "default": False
        }
    }

    # delete payload parser
    delete_parser = api.parser()
    delete_parser.add_argument('resource_ids', location='form', type=str, required=True)
    delete_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    delete_json_parse_directives = {
        "resource_ids": {"allowed_types": (list, )}
    }

    @api.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self):
        args = self.get_parser.parse_args()
        return get_requested_resource_jsons(args)

    @api.expect(post_parser, validate=True)
    @require_oauth()
    def post(self):
        args = self.post_parser.parse_args()
        args = parse_json_args(args, self.post_json_parse_directives)

        resources = args['resource_jsons']
        return_projection = args['return_projection']

        created_resources = []
        for n, r in enumerate(resources):
            try:
                if 'jsonClass' not in r:
                    raise ObjModelException('jsonClass attribute should exist for update/creation', 403)

                created_resource_id = objstore_helper.create_or_update(
                    g.objstore_colln, g.acl_svc, r,
                    current_token.user_id, current_token.team_ids, g.schema_validator,
                    initial_agents=g.initial_agents, upsert=args['upsert'])

                if created_resource_id is None:
                    created_resources.append(None)
                    continue
                created_resource_json = g.objstore_colln.get(
                    created_resource_id,
                    projection=projection_helper.modified_projection(return_projection, ["_id", "jsonClass"]))
                created_resources.append(created_resource_json)

            except ObjModelException as e:
                return error_response(
                    message='action not allowed at resource {}'.format(n),
                    code=e.http_response_code, details={"error": e.message})
            except (ValidationError, TypeError) as e:
                return error_response(
                    message='schema validation error at resource {}'.format(n),
                    code=400, details={"error": getattr(e, 'message', str(e))})

        return created_resources

    @api.expect(delete_parser, validate=True)
    @require_oauth()
    def delete(self):
        args = self.delete_parser.parse_args()
        args = parse_json_args(args, self.delete_json_parse_directives)

        resource_ids = args['resource_ids']

        ids_validity = False not in [isinstance(_id, str) for _id in resource_ids]
        if not ids_validity:
            return error_response(message='ids should be strings', code=404)

        found_nodes = g.objstore_colln.find({"_id": {"$in": resource_ids}})
        found_node_ids = [node['_id'] for node in found_nodes]
        non_existent_ids = [node_id for node_id in resource_ids if node_id not in found_node_ids]

        delete_status, all_deleted_ids = objstore_helper.delete_tree_new(
            g.objstore_colln, g.acl_svc, g.data_dir_path, found_nodes, current_token.user_id, current_token.team_ids)
        for node_id in non_existent_ids:
            delete_status[node_id] = None

        '''
        for resource_id in resource_ids:
            deleted, deleted_res_ids = objstore_helper.delete_tree(
                g.objstore_colln, g.acl_svc, g.data_dir_path,
                resource_id, current_token.user_id, current_token.team_ids
            )
            delete_report.append({
                "deleted": deleted,
                "deleted_resource_ids": deleted_res_ids
            })
        '''

        return {"delete_status": delete_status, "all_deleted_ids": all_deleted_ids}


# noinspection PyMethodMayBeStatic
@api.route('/resources/<string:resource_id>')
class ResourceObject(flask_restx.Resource):

    get_parser = api.parser()
    get_parser.add_argument('projection', location='args', type=str)
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )
    get_parser.add_argument(
        'attach_in_links', type=str, location='args', default='false', choices=['true', 'false']
    )


    get_payload_json_parse_directives = {
        "projection": {
            "allowed_types": (dict, ), "allow_none": True, "custom_validator": _validate_projection
        },
        "attach_in_links": {
            "allowed_types": (bool, ), "default": False
        }
    }

    @api.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self, resource_id):
        args = self.get_parser.parse_args()
        args = parse_json_args(args, self.get_payload_json_parse_directives)

        projection = args['projection']

        try:
            resource = objstore_helper.get_resource(
                g.objstore_colln, g.acl_svc,
                objstore_helper.resource_selector_doc(resource_id),
                current_token.user_id, current_token.team_ids
            )
        except objstore_helper.ObjModelException as e:
            return error_response(message=e.message, code=e.http_response_code)

        if not resource['resolvedPermissions'][PermissionResolver.READ]:
            return error_response(message='permission denied', code=403)

        if args['attach_in_links']:
            resource['_in_links'] = objstore_helper.get_in_links(g.objstore_colln, resource_id)

        projection_helper.project_doc(resource, projection, in_place=True)
        return resource


@api.route('/resources/<string:resource_id>/specific_resources')
class SpecificResources(flask_restx.Resource):

    get_parser = api.parser()
    get_parser.add_argument('filter_doc', location='args', type=str, default='{}')
    get_parser.add_argument('projection', location='args', type=str)
    get_parser.add_argument('start', location='args', type=int)
    get_parser.add_argument('count', location='args', type=int)
    get_parser.add_argument('sort_doc', location='args', type=str)
    get_parser.add_argument('return_count_only', location='args', type=str, choices=['true', 'false'], default='false')
    get_parser.add_argument(
        'attach_in_links', type=str, location='args', default='false', choices=['true', 'false']
    )
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    delete_parser = api.parser()
    delete_parser.add_argument('filter_doc', location='form', type=str)
    delete_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    delete_payload_json_parse_directives = {
        "filter_doc": {
            "allowed_types": (dict,), "default": {}
        }
    }

    @api.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self, resource_id):
        args = self.get_parser.parse_args()

        filter_doc = jsonify_argument(args['filter_doc'], key='filter_doc') or {}
        check_argument_type(filter_doc, (dict,), key='filter_doc')
        selector_doc = objstore_helper.specific_resources_selector_doc(resource_id, custom_filter_doc=filter_doc)
        args['selector_doc'] = json.dumps(selector_doc)

        return get_requested_resource_jsons(args)

    @api.expect(delete_parser, validate=True)
    @require_oauth()
    def delete(self, resource_id):
        args = self.delete_parser.parse_args()
        args = parse_json_args(args, self.delete_payload_json_parse_directives)

        filter_doc = args['filter_doc']
        selector_doc = objstore_helper.specific_resources_selector_doc(resource_id, custom_filter_doc=filter_doc)

        delete_status, all_deleted_ids = objstore_helper.delete_selection(
            g.objstore_colln, g.acl_svc, g.data_dir_path,
            selector_doc, current_token.user_id, current_token.team_ids
        )
        return {
            "delete_status": delete_status, "all_deleted_ids": all_deleted_ids
        }


@api.route('/resources/<string:resource_id>/annotations')
class Annotations(flask_restx.Resource):

    get_parser = api.parser()
    get_parser.add_argument('filter_doc', location='args', type=str, default='{}')
    get_parser.add_argument('projection', location='args', type=str)
    get_parser.add_argument('start', location='args', type=int)
    get_parser.add_argument('count', location='args', type=int)
    get_parser.add_argument('sort_doc', location='args', type=str)
    get_parser.add_argument('return_count_only', location='args', type=str, choices=['true', 'false'], default='false')
    get_parser.add_argument(
        'attach_in_links', type=str, location='args', default='false', choices=['true', 'false']
    )
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    delete_parser = api.parser()
    delete_parser.add_argument('filter_doc', location='form', type=str)
    delete_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    delete_payload_json_parse_directives = {
        "filter_doc": {
            "allowed_types": (dict,), "default": {}
        }
    }

    @api.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self, resource_id):
        args = self.get_parser.parse_args()

        filter_doc = jsonify_argument(args['filter_doc'], key='filter_doc') or {}
        check_argument_type(filter_doc, (dict,), key='filter_doc')
        selector_doc = objstore_helper.annotations_selector_doc(resource_id, custom_filter_doc=filter_doc)
        args['selector_doc'] = json.dumps(selector_doc)

        return get_requested_resource_jsons(args)

    @api.expect(delete_parser, validate=True)
    @require_oauth()
    def delete(self, resource_id):
        args = self.delete_parser.parse_args()
        args = parse_json_args(args, self.delete_payload_json_parse_directives)

        filter_doc = args['filter_doc']
        selector_doc = objstore_helper.annotations_selector_doc(resource_id, custom_filter_doc=filter_doc)

        delete_status, all_deleted_ids = objstore_helper.delete_selection(
            g.objstore_colln, g.acl_svc, g.data_dir_path,
            selector_doc, current_token.user_id, current_token.team_ids
        )
        return {
            "delete_status": delete_status, "all_deleted_ids": all_deleted_ids
        }


# noinspection PyMethodMayBeStatic
@api.route('/files/<string:file_id>')
class File(flask_restx.Resource):

    get_parser = api.parser()
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    @api.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self, file_id):
        ool_data = g.objstore_colln.find_one({"_id": file_id})
        if ool_data is None:
            return error_response(message="file not found", code=404)

        '''TODO
        if not PermissionResolver.resolve_permission(
                ool_data, PermissionResolver.READ, current_token.user_id, current_token.team_ids, g.objstore_colln):
            return error_response(message='permission denied', code=403)
        '''

        namespace = ool_data.get('namespace', None)
        identifier = ool_data.get('identifier', None)
        url = ool_data.get('url', None)

        if None in (namespace, identifier):
            return error_response(message='resource is not ool_data', code=404)

        if namespace == '_vedavaapi':
            local_namespace_data = ool_data.get('namespaceData', {})
            mimetype = local_namespace_data.get('mimetype', None)
            # print({"mimetype": mimetype}, file=sys.stderr)

            file_path = os.path.join(g.data_dir_path, identifier.lstrip('/'))
            if not os.path.isfile(file_path):
                return error_response(message='no representation available', code=404)

            file_dir = os.path.dirname(file_path)
            file_name = os.path.basename(file_path)

            from flask import send_from_directory
            return send_from_directory(
                directory=file_dir, filename=file_name, mimetype=mimetype
            )
        elif namespace == '_abstract':
            abstract_file_resolver = g.abstract_file_resolver  # type: AbstractFileResolver
            # noinspection PyUnusedLocal
            agent_context = {
                'user_id': current_token.user_id, 'team_ids': current_token.team_ids}
            try:
                af = abstract_file_resolver.get(ool_data, agent_context=None)  # TODO
                return flask.send_file(
                    af.cached_file_path, mimetype=af.mimetype
                )
            except ObjModelException as e:
                return error_response(message=e.message, code=e.http_response_code)
        else:
            if url is None:
                return error_response(message='resource is not ool_data', code=404)
            return flask.redirect(url)


# noinspection PyMethodMayBeStatic
@api.route('/graph')
class Graph(flask_restx.Resource):

    get_parser = api.parser()
    get_parser.add_argument(
        'start_nodes_selector', type=str, location='args', required=True
    )
    get_parser.add_argument(
        'traverse_key_filter_maps_list', type=str, location='args', default='[{"source": {}, "target": {}}]'
    )
    get_parser.add_argument(
        'hop_inclusions_config', type=str, location='args'
    )
    get_parser.add_argument(
        'include_incomplete_paths', type=str, location='args', choices=['true', 'false'], default='false'
    )
    get_parser.add_argument(
        'direction', type=str, location='args', choices=['referred', 'referrer'], required=True
    )
    get_parser.add_argument(
        'max_hops', type=int, location='args', default=0
    )
    get_parser.add_argument(
        'include_ool_data_graph', type=str, location='args', choices=['true', 'false'], default='false'
    )
    get_parser.add_argument(
        'include_acls_graph', type=str, location='args', choices=['true', 'false'], default='false'
    )
    get_parser.add_argument(
        'json_class_projection_map', type=str, location='args', default=None
    )
    get_parser.add_argument(
        'attach_in_links', type=str, location='args', default='false', choices=['true', 'false']
    )
    get_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    get_payload_json_parse_directives = {
        "start_nodes_selector": {"allowed_types": (dict, )},
        "start_nodes_sort_doc": {"allowed_types": (list, ), "allow_none": True},
        "traverse_key_filter_maps_list": {"allowed_types": (list, )},
        "hop_inclusions_config": {"allowed_types": (list, ), "default": [True]},
        "include_incomplete_paths": {"allowed_types": (bool, ), "default": False},
        "include_ool_data_graph": {"allowed_types": (bool, ), "default": False},
        "include_acls_graph": {"allowed_types": (bool, ), "default": False},
        "attach_in_links": {"allowed_types": (bool, ), "default": False},
        "json_class_projection_map": {"allowed_types": (dict, ), "default": {"*": None}},
    }

    put_parser = api.parser()
    put_parser.add_argument(
        'start_nodes_selector', type=str, location='form', required=True
    )
    put_parser.add_argument(
        'traverse_key_filter_maps_list', type=str, location='form', default='[{"source": {}, "target": {}}]'
    )
    put_parser.add_argument(
        'hop_inclusions_config', type=str, location='form'
    )
    put_parser.add_argument(
        'include_incomplete_paths', type=str, location='form', choices=['true', 'false'], default='false'
    )
    put_parser.add_argument(
        'direction', type=str, location='form', choices=['referred', 'referrer'], required=True
    )
    put_parser.add_argument(
        'max_hops', type=int, location='form', default=0
    )
    put_parser.add_argument(
        'include_ool_data_graph', type=str, location='form', choices=['true', 'false'], default='false'
    )
    put_parser.add_argument(
        'include_acls_graph', type=str, location='form', choices=['true', 'false'], default='false'
    )
    put_parser.add_argument(
        'json_class_projection_map', type=str, location='form', default=None
    )
    put_parser.add_argument(
        'attach_in_links', type=str, location='form', default='false', choices=['true', 'false']
    )
    put_parser.add_argument(
        'Authorization', location='headers', type=str, required=False,
        help='should be in form of "Bearer <access_token>"'
    )

    post_parser = api.parser()
    post_parser.add_argument(
        'graph', type=str, location='form', required=True
    )
    post_parser.add_argument(
        'ool_data_graph', type=str, location='form', required=False
    )
    post_parser.add_argument(
        'files', type=FileStorage, location='files'
    )
    post_parser.add_argument(
        'should_return_resources', type=str, location='form', choices=['true', 'false'], default='false'
    )
    post_parser.add_argument(
        'should_return_oold_resources', type=str, location='form', choices=['true', 'false'], default='false'
    )
    post_parser.add_argument(
        'response_projection_map', type=str, location='form'
    )
    post_parser.add_argument(
        'upsert', type=str, location='form', choices=['true', 'false'], default='false'
    )
    post_parser.add_argument(
        'Authorization', location='headers', type=str, required=True,
        help='should be in form of "Bearer <access_token>"'
    )

    post_payload_json_parse_directives = {
        "graph": {"allowed_types": (dict, )},
        "ool_data_graph": {"allowed_types": (dict, ), "default": {}},
        "should_return_resources": {"allowed_types": (bool, ), "default": False},
        "should_return_oold_resources": {"allowed_types": (bool, ), "default": False},
        "response_projection_map": {"allowed_types": (dict, ), "default": {}},
        "upsert": {"allowed_types": (bool, ), "default": False}
    }

    def _get(self, args):

        start_nodes_selector = args['start_nodes_selector']

        traverse_key_filter_maps_list = args['traverse_key_filter_maps_list']

        for kf_map in traverse_key_filter_maps_list:
            if not isinstance(kf_map, dict):
                return error_response(message='invalid traverse_key_filter_maps_list', code=400)

        hop_inclusions_config = args['hop_inclusions_config']

        include_incomplete_paths = args['include_incomplete_paths']

        json_class_projection_map = args['json_class_projection_map']
        include_ool_data_graph = args['include_ool_data_graph']
        include_acls_graph = args['include_acls_graph']

        direction = args.get('direction')
        max_hops = args.get('max_hops', 0)
        try:
            acls_graph = {}
            start_nodes = objstore_helper.get_read_permitted_resources(
                g.objstore_colln, g.acl_svc, current_token.user_id, current_token.team_ids,
                start_nodes_selector, acls_cache=acls_graph)

            graph, acls_graph, start_nodes_ids, collected_node_ids = objstore_graph_helper.get_graph(
                g.objstore_colln, g.acl_svc, start_nodes, traverse_key_filter_maps_list, direction,
                max_hops, current_token.user_id, current_token.team_ids,
                hop_inclusions_config=hop_inclusions_config, include_incomplete_paths=include_incomplete_paths,
                acls_cache=acls_graph
            )
        except objstore_helper.ObjModelException as e:
            return error_response(message=str(e), code=e.http_response_code)
        except objstore_graph_helper.GraphValidationError as e:
            return error_response(message=str(e), error=str(e.error), code=e.http_status_code)

        projected_graph = dict((k, graph[k]) for k in collected_node_ids)

        objstore_graph_helper.project_graph_nodes(projected_graph, json_class_projection_map, in_place=True)
        if args['attach_in_links']:
            for rid, r in projected_graph.items():
                r['_in_links'] = objstore_helper.get_in_links(g.objstore_colln, rid)

        response = {
            "graph": projected_graph, "start_nodes_ids": start_nodes_ids,
            #  "graph_order": collected_node_ids  # TODO
        }

        if include_ool_data_graph:
            ool_data_graph = objstore_graph_helper.get_ool_data_graph(
                g.objstore_colln, g.acl_svc, projected_graph, current_token.user_id, current_token.team_ids)

            objstore_graph_helper.project_graph_nodes(ool_data_graph, json_class_projection_map, in_place=True)
            response['ool_data_graph'] = ool_data_graph

        if include_acls_graph:
            for k in list(acls_graph.keys()):
                if k not in projected_graph:
                    del acls_graph[k]
            response['acls_graph'] = acls_graph

        return response, 200

    @api.expect(get_parser, validate=True)
    @require_oauth(token_required=False)
    def get(self):
        args = parse_json_args(
            self.get_parser.parse_args(), self.get_payload_json_parse_directives)
        return self._get(args)

    @api.expect(put_parser, validate=True)
    @require_oauth(token_required=False)
    def put(self):
        args = parse_json_args(
            self.put_parser.parse_args(), self.get_payload_json_parse_directives)
        return self._get(args)

    @api.expect(post_parser, validate=True)
    @require_oauth()
    def post(self):
        args = parse_json_args(
            self.post_parser.parse_args(), self.post_payload_json_parse_directives)
                                  
        graph = args['graph']
        ool_data_graph = args['ool_data_graph']

        should_return_resources = args['should_return_resources']
        should_return_oold_resources = args['should_return_oold_resources']

        response_projection_map = args['response_projection_map']

        for json_class in response_projection_map.keys():
            _validate_projection(response_projection_map[json_class])

        files = request.files.getlist("files")
        files_map = dict((f.filename, f) for f in files)

        try:
            graph_ids_to_uids_map, ool_data_graph_ids_to_uids_map = objstore_graph_helper.post_graph_with_ool_data(
                g.objstore_colln, g.acl_svc, g.data_dir_path,
                current_token.user_id, current_token.team_ids,
                graph, ool_data_graph, files_map, g.schema_validator,
                initial_agents=g.initial_agents, upsert=args['upsert']
            )
        except objstore_graph_helper.GraphValidationError as e:
            return error_response(
                message=str(e),
                code=e.http_status_code,
                error=str(e.error)
            )

        response = {}
        if should_return_resources:
            response['graph'] = objstore_graph_helper.get_projected_graph_from_ids_map(
                g.objstore_colln, graph_ids_to_uids_map, response_projection_map
            )
        else:
            response['graph'] = graph_ids_to_uids_map

        if should_return_oold_resources:
            response['ool_data_graph'] = objstore_graph_helper.get_projected_graph_from_ids_map(
                g.objstore_colln, ool_data_graph_ids_to_uids_map, response_projection_map
            )
        else:
            response['ool_data_graph'] = ool_data_graph_ids_to_uids_map

        return response, 200


@api.route('/schemas/<json_class>')
class Schema(flask_restx.Resource):

    # noinspection PyMethodMayBeStatic
    def get(self, json_class):
        schema_validator = g.schema_validator  # type: SchemaValidator
        vv_schemas = schema_validator.vv_schemas  # type JsonClassDefs
        schema = vv_schemas.get(json_class)
        if not schema:
            return error_response(message='json_class not defined', code=404)
        return schema
