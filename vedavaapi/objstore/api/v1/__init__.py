import flask_restx
from flask import Blueprint
from .. import myservice

api_blueprint_v1 = Blueprint('objstore' + '_v1', __name__)

api = flask_restx.Api(
    app=api_blueprint_v1,
    version='1.0',
    title=myservice().title,
    doc='/docs'
)

from . import rest
