import os
from collections import namedtuple

import requests
from flask import g
from requests import HTTPError

from vedavaapi.common.helpers.api_helper import get_current_org

from . import myservice


def _get_acl_svc():
    current_org_name = get_current_org()
    acls_service = myservice().registry.lookup('acls')
    return acls_service.get_acl_svc(current_org_name)


def _get_abstract_file_resolver():
    current_org_name = get_current_org()
    abstractfs_svc = myservice().registry.lookup('abstractfs')
    if not abstractfs_svc:
        return None
    # noinspection PyUnresolvedReferences
    from vedavaapi.abstractfs.lib import AbstractFileResolver
    return abstractfs_svc.resolver(current_org_name)  # type: AbstractFileResolver


def _get_objstore_colln():
    org_name = get_current_org()
    return myservice().colln(org_name)


def _get_data_dir_path():
    org_name = get_current_org()
    return myservice().data_dir_path(org_name)


def _get_schema_validator():
    current_org_name = get_current_org()
    schema_service = myservice().registry.lookup('schemas')
    return schema_service.get_schema_validator(current_org_name, _get_objstore_colln())


def _get_token_introspection_endpoint():
    current_org_name = get_current_org()
    accounts_api_config = myservice().get_accounts_api_config(current_org_name)

    url_root = accounts_api_config.get('url_root', g.original_url_root)
    token_resolver_endpoint = os.path.join(
        url_root.lstrip('/'),
        current_org_name,
        'accounts/v1/oauth/introspect_token'
    )
    return token_resolver_endpoint


def _get_initial_agents():
    current_org_name = get_current_org()
    initial_agents = myservice().get_initial_agents(current_org_name)
    if initial_agents is not None:
        return initial_agents

    accounts_api_config = myservice().get_accounts_api_config(current_org_name)
    url_root = accounts_api_config.get('url_root', g.original_url_root)
    initial_agents_endpoint = os.path.join(
        url_root.lstrip('/'),
        current_org_name,
        'accounts/v1/setup/initial_agents'
    )

    try:
        response = requests.get(initial_agents_endpoint)
        try:
            response.raise_for_status()
        except HTTPError:
            return None
        initial_agents_json = response.json()

        InitialAgents = namedtuple('InitialAgents', ['all_users_team_id', 'root_admins_team_id'])
        initial_agents = InitialAgents(
            initial_agents_json.get('all_users_team_id', None), initial_agents_json.get('root_admins_team_id', None))
        myservice().set_initial_agents(current_org_name, initial_agents)
    except Exception:
        from vedavaapi.common.helpers.api_helper import get_initial_agents
        initial_agents = get_initial_agents(current_org_name)

    return initial_agents


def push_environ_to_g():
    g.token_introspection_endpoint = _get_token_introspection_endpoint()
    g.current_org_name = get_current_org()
    g.objstore_colln = _get_objstore_colln()
    g.data_dir_path = _get_data_dir_path()
    g.initial_agents = _get_initial_agents()
    g.abstract_file_resolver = _get_abstract_file_resolver()
    g.schema_validator = _get_schema_validator()
    g.acl_svc = _get_acl_svc()
